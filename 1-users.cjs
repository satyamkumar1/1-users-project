const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/


// Q1 Find all users who are interested in playing video games.



function getUserPlayingVideoGame() {
    const getData = Object.entries(users);

    const getUser = getData.filter((data) => {

        const { ...arr } = data[1];
        console.log(arr.interests);
        arr.interests.filter((item) => {

            //if(item=="Video Games" || item=="Playing Video Games")
            if (item.includes("Video Games")) {
                return data;
            }

        })
    });

    console.log(getUser);
}
getUserPlayingVideoGame();



// Q2 Find all users staying in Germany.


function userInGermany() {

    const userGermany = Object.entries(users);

    const getUserData = userGermany.filter((data) => {
        //console.log(data);
        if (data[1].nationality == "Germany") {
            return data;
        }
    })
    console.log(getUserData);

}


userInGermany();



// Q3 Sort users based on their seniority level.


function sortUserBasedOnSeniority() {

    const sortData = Object.keys(users);

    const getsortedData = sortData.sort((user1, user2) => {

        const prevUser = users[user1].desgination;
        const currUser = users[user2].desgination;

        console.log(prevUser, currUser);

        let prevUserAge = users[user1].age;
        let currUserAge = users[user2].age;


        if (prevUser > currUser && prevUserAge < currUserAge) {
            return -1;
        }
        else if (prevUser < currUser && prevUserAge > currUserAge) {
            return 1;
        }
        else {
            return 0;
        }



    })

    console.log(getsortedData);

}
sortUserBasedOnSeniority();






// Q4 Find all users with masters Degree.


function getMasterDegree() {

    const userMaster = Object.entries(users);

    const getUserData = userMaster.filter((data) => {
        //console.log(data);
        if (data[1].qualification == "Masters") {
            return data;
        }
    })
    console.log(getUserData);


}
getMasterDegree();



// Q5 Group users based on their Programming language mentioned in their designation.


function groupUserOnDesgnition() {

    const groupUser = Object.entries(users);

    const groupUserData = groupUser.reduce((accum, data) => {

        if (accum[data[1].desgination]) {
            accum[data[1].desgination].push(data[1]);
        }
        else {
            accum[data[1].desgination] = data[1];
        }
        return accum;

    }, {});

    console.log(groupUserData);
}
groupUserOnDesgnition();